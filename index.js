require('dotenv').config()

const express = require("express")

const Router = require("./config/routes")

const app = express()
const port = process.env.PORT


app.use(express.static("public"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(Router);

app.listen(port, () => {
    console.log(`server running on http://localhost:${port}`)
})
const router = require('express').Router()
const users = require('./users')
const cars = require('./cars')
const swaggerUi = require('swagger-ui-express')
const swaggerDocs = require('../../docs/swagger.json')

// API
router.use('/api/users', users)
router.use('/api/cars', cars)

// swagger
router.use('/api-docs', swaggerUi.serve)
router.get('/api-docs', swaggerUi.setup(swaggerDocs))

module.exports = router
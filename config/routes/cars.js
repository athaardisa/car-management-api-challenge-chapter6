const router = require('express').Router()
const isAdmin = require('../../middleware/isAdmin')
const { createCars, getCars, getCarsMember, updateCars, deleteCars } = require('../../app/controllers/carsController')
const auth = require('../../middleware/auth')


router.get('/', auth, getCarsMember)
router.get('/admin', auth, isAdmin, getCars)
router.post('/add', auth, isAdmin, createCars)
router.put("/update/:id", auth, isAdmin, updateCars);
router.delete("/delete/:id", auth, isAdmin, deleteCars);

module.exports = router
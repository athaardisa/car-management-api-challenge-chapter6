const router = require('express').Router()
const { login, register, toAdmin, currentUser } = require('../../app/controllers/userController')
const isSuperAdmin = require('../../middleware/isSuperAdmin')
const auth = require('../../middleware/auth')

router.post('/login', login)
router.post('/register', register)
router.get('/currentUser', auth, currentUser)
router.post('/makeAdmin', auth, isSuperAdmin, toAdmin)

module.exports = router
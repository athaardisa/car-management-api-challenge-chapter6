const jwt = require("jsonwebtoken");
const { users } = require("../app/models");

module.exports = function (req, res, next) {
  // req is an object

  // Client will headers called authorization which contains JWT
  try {
    const bearerToken = req.headers.authorization; // Basic Authentication -> Bearer Authentication
    const token = bearerToken.split("Bearer ")[1]

    if (!token) {
      res.status(401).json({
        status: "fail",
        errors: "required authorization",
      });
    }

    const payload = jwt.verify(token, "rahasia");
    users.findByPk(payload.id).then((instance) => {
      req.user = instance;
      next();
    });
  } catch {
    res.status(401).json({
      status: "fail",
      errors: "Invalid Token",
    });
  }
};

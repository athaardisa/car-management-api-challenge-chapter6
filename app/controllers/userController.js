const user = require("../services/userService");

const currentUser = async (req, res, next) => {
  res.status(200).json({
      status: 'Success',
      user: {
          id: req.user.id,
          name: req.user.name,
          email: req.user.email,
          roleId: req.user.roleId,
      }
  })
}

const login = async (req, res) => {
  try {
    const token = await user.login(req.body);
    console.log(token)
    res.status(201).json({
      status: "success",
      message: "login success",
      token,
    });
  } catch (error) {
    res.status(error.statusCode).json({
      status: "fail",
      message: error.message,
    });
  }
};

const register = async (req, res) => {
  try {
    const newUser = await user.register(req.body);
    res.status(201).json({
      status: "success",
      message: "success register",
      newUser,
    });
  } catch (error) {
    res.status(error.statusCode || 404).json({
      status: "fail",
      message: error.message,
    });
  }
};

const toAdmin = async (req, res) => {
    try {
      const newAdmin = await user.toAdmin(req.body);
      res.status(201).json({
        status: "success",
        message: "update to admin success",
        newAdmin,
      });
    } catch (error) {
      res.status(error.statusCode || 404).json({
        status: "fail",
        message: error.message,
      });
    }
  };

  module.exports = {
    login,
    register,
    toAdmin,
    currentUser
  }
const cars = require("../services/carsService");

// create car
const createCars = async (req, res) => {
  try {
    const car = await cars.createCars(req.body, req.user.id);
    res.status(201).json({
      status: "success",
      message: "data added!",
      car,
    });
  } catch (error) {
    res.status(404).json({
      status: "fail",
      message: "add data fail",
    });
  }
};

// get car for admin
const getCars = async (req, res) => {
  try {
    const car = await cars.getCars();
    res.status(201).json({
      status: "success",
      car,
    });
  } catch (error) {
    res.status(404).json({
      status: "fail",
      error: error.message,
    });
  }
};

// get car member
const getCarsMember = async (req, res) => {
  try {
    const car = await cars.getCarsMember();
    res.status(201).json({
      status: "success",
      car,
    });
  } catch (error) {
    res.status(404).json({
      status: "fail",
      error: error.message,
    });
  }
};

// update cars
const updateCars = async (req, res) => {
  try {
    const carId = req.params.id
    const car = await cars.updateCars(carId, req.body, req.user.id);
    res.status(201).json({
      status: "success",
      message: "data updated!",
      car,
    });
  } catch (error) {
    res.status(404).json({
      status: "fail",
      message: "update data fail",
    });
  }
};

// delete cars
const deleteCars = async (req, res) => {
  try {
    const carId = req.params.id
    await cars.deleteCars(req.user.id, carId);
    res.status(201).json({
      status: "success",
      message: "data deleted!",
    });
  } catch (error) {
    res.status(404).json({
      status: "fail",
      message: "delete data fail",
    });
  }
};

module.exports = {
    createCars,
    getCars,
    getCarsMember,
    updateCars,
    deleteCars
}
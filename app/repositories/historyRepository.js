const { history } = require("../models");

// create
const create = async (reqBody) => {
  return await history.create({
    createdBy: reqBody,
  });
};

// get
// const get = async () => {
//   const getHistory = await history.findAll();
//   const id = getHistory.length - 1;
//   return history.findOne({
//     where: {
//       id,
//     },
//   });
// };

// updated by
const updateBy = async (reqBody, id) => {
  return await history.update(
    {
      updatedBy: reqBody,
    },
    {
      where: {
        id,
      },
    }
  );
};

// deleted by
const deleteBy = async (reqBody, id) => {
  return await history.update(
    {
      deletedBy: reqBody,
    },
    {
      where: {
        id,
      },
    }
  );
};

module.exports = {
    create,
    // get,
    updateBy,
    deleteBy
}
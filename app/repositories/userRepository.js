const { users } = require("../models");

// check email
const checkEmail = (email) => {
  return users.findOne({
    where: {
      email,
    },
  });
};

// create user
const createUser = async (reqBody) => {
  return await users.create(reqBody);
};

// get user by email
const getUserEmail = async (email) => {
  return users.findOne({
    where: {
      email,
    },
  });
};

// get user by id
const getUserId = async(id) => {
  return users.findOne({
    where: {
      id
    }
  })
}

// update member to admin
const updateToAdmin = async (user) => {
  const { name, email, password } = user;
  await users.update(
    { name, email, password, roleId: 2 },
    {
      where: {
        name,
      },
    }
  );
};

module.exports = {
  checkEmail,
  createUser,
  getUserEmail,
  getUserId,
  updateToAdmin,
};

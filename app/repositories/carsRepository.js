const { cars } = require("../models");
const { history } = require("../models");

// create cars
const createCars = async (reqBody) => {
  return await cars.create(reqBody);
};

// get cars for admin
const getCars = () => {
  return cars.findAll({
    include: {
      model: history,
    },
  });
};

// get cars member
const getCarsMember = () => {
  return cars.findAll(
    {
      attributes: {
        exclude: ["historyId"],
      },
    }
  );
};

// get cars by id
const getCarsById = (id) => {
  return cars.findOne({
    where: {
      id,
    },
  });
};

// update cars
const updateCars = async (id, reqBody) => {
  return await cars.update(reqBody, {
    where: {
      id,
    },
  });
};

// delete cars
const deleteCars = async (id) => {
  return await cars.update(
    {
      isActive: false,
    },
    {
      where: {
        id,
      },
    }
  );
};

module.exports = {
  createCars,
  getCars,
  getCarsMember,
  getCarsById,
  updateCars,
  deleteCars,
};

const repo = require('../repositories/carsRepository')
const { getUserId } = require('../repositories/userRepository')
const {create, updateBy, deleteBy} = require('../repositories/historyRepository')
const ApiError = require('../../utils/ApiError')

// create cars
const createCars = async (reqBody, userId) => {
    // mencari nama berdasarkan id yg dikirim dari autentikasi
    const userLogin = await getUserId(userId)
    // memasukan nama pembuat data ke tabel history
    const createdBy = await create(userLogin.email)
    // mendapatkan id tabel history untuk dimasukkan ke field historyId
    const historyId = createdBy.id
    // mendapatkan data dari inputan user
    const { name, price, size } = reqBody
    const reqbody = { name, price, size, isActive: true, historyId }
    return await repo.createCars(reqbody)
}

// get cars for admin
const getCars = async () => {
    const cars = await repo.getCars()
    if(!cars){
        throw new ApiError()
    }
    return cars
}

// get cars for member
const getCarsMember = async () => {
    const cars = await repo.getCarsMember()
    if(!cars){
        throw new ApiError()
    }
    const carsAvailable = cars.filter((car) => car.isActive == true);
    return carsAvailable
}

// update cars
const updateCars = async (carsId, reqBody, userId) => {
    const userLogin = await getUserId(userId)
    const{ name, price, size } = reqBody
    const reqbody = { name, price, size }
    const carId = await repo.getCarsById(carsId)
    await updateBy(userLogin.email, carId.historyId)
    await repo.updateCars(carsId, reqbody)
    return carId
}

// delete cars
const deleteCars = async (userId, carsId) => {
    const userLogin = await getUserId(userId)
    const CarId = await repo.getCarsById(carsId)
    await deleteBy(userLogin.email, CarId.historyId)
    return await repo.deleteCars(carsId)
}

module.exports = {
    createCars,
    getCars,
    getCarsMember,
    updateCars,
    deleteCars
}
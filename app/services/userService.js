const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const ApiError = require("../../utils/ApiError");
const {
  checkEmail,
  createUser,
  getUserEmail,
  updateToAdmin,
} = require("../repositories/userRepository");

const login = async (reqBody) => {
  const { email, password } = reqBody;
  const user = await checkEmail(email);
  // console.log(user)
  if (!user) throw new ApiError(404, "email not found");

  if (bcrypt.compareSync(password, user.password)) {
    const token = jwt.sign({ id: user.id }, "rahasia");
    return token;
  } else {
    throw new ApiError(400, "Wrong Password");
  }
};

const register = async (reqBody) => {
  const { name, email, password } = reqBody;

  if (!email) throw new ApiError(400, "email cannot be empty");
  if (!name) throw new ApiError(400, "name cannot be empty");
  if (!password) throw new ApiError(400, "password cannot be empty");

  // validasi jika email sudah terpakai
  const user = await checkEmail(email);
  if (user) throw new ApiError(400, "email alredy exist!");

  // validasi minimum password length
  if (password.length < 8)
    throw new ApiError(
      400,
      "minimum password length must be 8 character or more"
    );

  // hash password
  const hashedPass = bcrypt.hashSync(password, 10);
  const newUser = {
    name,
    email,
    password: hashedPass,
    roleId: 1,
  };
  return createUser(newUser);
};

// update member to admin
const toAdmin = async (reqBody) => {
  const user = await getUserEmail(reqBody.email);
  if (!user){
    throw new ApiError(404, `email ${reqBody.email} not found`);
  } 
  if (user.roleId == 2){
    throw new ApiError(400, `email ${reqBody.email} alredy admin`);
  }
  if (reqBody.role != "admin"){
    throw new ApiError(400, `can only update to admin`);
  }

  return await updateToAdmin(user);
};

module.exports = {
  login,
  register,
  toAdmin,
};
